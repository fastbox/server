'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

/** Authentication Routes */
Route.post('/signup', 'UserController.signup')
Route.post('/signin', 'UserController.signin')

Route.resource('products', 'ProductController')
  .apiOnly()
  .middleware('auth')

Route.post('/categories/:id/products', 'ProductController.store')
  .middleware('auth')

Route.resource('categories', 'CategoryController')
  .apiOnly()
  .middleware('auth')

Route.resource('companies', 'CompanyController')
  .apiOnly()
  .middleware('auth')

//Images Routes
Route.post('/users/:id/image', 'ImageController.storeUserImage')
  .middleware('auth')

Route.post('/categories/:id/image', 'ImageController.storeCategoryImage')
  .middleware('auth')

Route.post('/products/:id/image', 'ImageController.storeProductImage')
  .middleware('auth')

Route.get('/images/users/:user_id/:path', 'ImageController.showUserImage')
Route.get('/images/categories/:category_id/:path', 'ImageController.showCategoryImage')
Route.get('/images/products/:product_id/:path', 'ImageController.showProductImage')
