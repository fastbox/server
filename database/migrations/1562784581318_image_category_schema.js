'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ImageCategorySchema extends Schema {
  up () {
    this.create('image_categories', (table) => {
      table.increments()
      table.string('path').notNullable()
      table
        .integer('category_id')
        .unsigned()
        .references('id')
        .inTable('categories')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('image_categories')
  }
}

module.exports = ImageCategorySchema
