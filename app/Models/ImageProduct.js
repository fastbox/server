'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Env = use('Env')
const Model = use('Model')

class ImageProduct extends Model {
  static get computed () {
    return ['url']
  }

  getUrl ({ product_id, path }) {
    return `${Env.get('APP_STORAGE_URL')}/images/products/${product_id}/${path}`
  }
}

module.exports = ImageProduct
