'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Env = use('Env')
const Model = use('Model')

class ImageCategory extends Model {
  static get computed () {
    return ['url']
  }

  getUrl ({ category_id, path }) {
    return `${Env.get('APP_STORAGE_URL')}/images/categories/${category_id}/${path}`
  }
}

module.exports = ImageCategory
