'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Env = use('Env')
const Model = use('Model')

class ImageUser extends Model {
  static get computed () {
    return ['url']
  }

  getUrl ({ user_id, path }) {
    return `${Env.get('APP_STORAGE_URL')}/images/users/${user_id}/${path}`
  }
}

module.exports = ImageUser
