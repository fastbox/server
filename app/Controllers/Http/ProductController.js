'use strict'

const Product = use('App/Models/Product')
const Category = use('App/Models/Category')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with products
 */
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const products = Product.query()
    .with('images')
    .fetch()

    return products
  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   */
  async store ({ request, params }) {
    const category = await Category.findOrFail(params.id)

     const { id } = category

    const data = request.only([
      'title',
      'price',
      'qtd',
      'description'
    ])

    const product = await Product.create({ ...data, category_id: id })

    return product

  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params }) {
    const product = await Product.findOrFail(params.id)

    await product.load('images')

    return product

  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request }) {
    const product = await Product.findOrFail(params.id)

    const data = request.only([
      'title',
      'price',
      'description'
    ])

    product.merge(data)

    await product.save()

    return product
  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async destroy ({ params, auth, response }) {
    const product = await Product.findOrFail(params.id)

    if (auth.user.type !== 'admin') {
      return response.status(401).send({
        error: 'Você não possui permissão.'
      })
    }

    await product.delete()
  }
}

module.exports = ProductController
