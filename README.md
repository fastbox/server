# Fastbox API
**Apresentação do projeto:** [link](https://docs.google.com/presentation/d/1CDIb2Xd654DkUu5mjfbvlpWxYeiGsOUDuPxpSGVYheA/edit?usp=sharing)

<h4>O que é uma API?</h4>

API é um conjunto de rotinas e padrões de programação para acesso a um aplicativo de software ou plataforma baseado na Web. A sigla API refere-se ao termo em inglês "Application Programming Interface" que significa em tradução para o português "Interface de Programação de Aplicativos".


<h4>Demonstração</h4>

**O site e API estão disponibilizados no heroku, abaixo algumas informações para teste.**

**Like para o sistema:** [https://fastbox.herokuapp.com/](https://fastbox.herokuapp.com/)

**Link API**: [http://fastbox-server.herokuapp.com/](http://fastbox-server.herokuapp.com/)

**Arquivos de teste HTTP:** [insomnia-2019-07-12.json](https://drive.google.com/file/d/1xkubMUIi8vd3lvZAj8Nc2IoE7IePpjEC/view?usp=sharing)

faça o download do arquivo e importe em seu programa de requisições.

<h4>Tecnologias e implementação</h4>

**Estes são os passos para criar uma API em AdonisJS, a mesma vem pré-configurada com.**

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use o comando adonis para instalar o blueprint

```bash

npm i -g @adonisjs/cli

adonis new myserver --api-only

cd myserver

adonis serve

```

ou faça um clone do repo e execute o comando `npm install` ou `yarn install`.


### Migrations

Para rodar as migrations você precisa ter seu arquivo .env criado, nele sua base de datos deve estar configurada. O adonis vem pré-configurado com sqlite, porém você pode alterar.

**Documentação db driver:** [https://adonisjs.com/docs/4.1/database](https://adonisjs.com/docs/4.1/database)

exemplo .env

```env
HOST=127.0.0.1
PORT=3333
NODE_ENV=development
APP_NAME=NomeDaAplicação
APP_URL=http://${HOST}:${PORT}
CACHE_VIEWS=false
APP_KEY=
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=
DB_DATABASE=NomeDoBanco
HASH_DRIVER=bcrypt

```
No caso acima esta sendo utilizado o mysql, sendo assim o driver do mesmo deve ser adicionado as dependencias do projeto.

```bash
npm i mysql
```

ou

```bash
npm i mysql2
```

Assim que o banco estiver configurado basta executar o comando...

```bash
adonis migration:run
```

